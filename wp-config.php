<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'soundpays' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b>AAa[|EdJ6<9FEQ]PZ{F/{60MrgqoxtUr7Q:hBU{5k1.W,JTZ`k%`%6V>@a@IK|' );
define( 'SECURE_AUTH_KEY',  'pKamXtJ1G?&KI DF/RI!$(Kx7nF>ZYs< 8%qKVqvYB[G5h>29r@dZj/h05Jk|Z0Z' );
define( 'LOGGED_IN_KEY',    ' c`64s)or]mA_i/P%/H1$3=asK8FIYhfKK!~X|x/R{LvtCkio![6Z~po_?JYpTjX' );
define( 'NONCE_KEY',        'Lj]hr`C&9)6V2)Lmr/4U;W_x20V<W,uAbhORABHSCB6<?b!3_QY]R98p!A~,.U_B' );
define( 'AUTH_SALT',        'ZL38ifw$wrN0-!,OwbH?dSt8~(dz/}}o(9a|D2%0aOF5l;bHN/{+wY^]I9[f&#tN' );
define( 'SECURE_AUTH_SALT', 'F#wuEk>]pKc_6`9[~Lyd-3VafDc6Hc7EZ)UmEoxG-C[8O;@d}q;>z!?uD?6v)$ix' );
define( 'LOGGED_IN_SALT',   'cdxS* ;xn(}bO6CyB*:ggiJ;/n^<QTT JWk$u>`a{`.~0y?Uz=G.14QL&WbW:(i ' );
define( 'NONCE_SALT',       'VQ9y>&M<njp*&7,63kRih`L*,a<8mJlx*7S4LiutwQD7O4O 96S=Agl&PGo#mEC#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
